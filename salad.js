let salads = [
    {
        "name": "Green salad with tomatoe",
        "prize": 4,
        "id": 1,
        "ingredients": [
            "Iceberg",
            "lettuce",
            "tomatoes"
        ],
        "imageUrl": "salad/GreenSaladWithTomato.jpg"
    },
    {
        "name": "Tomato salad with mozzarella",
        "prize": 5,
        "id": 2,
        "ingredients": [
            "Tomato",
            "mozzarella"
        ],
        "imageUrl": "salad/TomatoSaladWithMozzarella.jpg"
    },
    {
        "name": "Field salad with egg",
        "prize": 4,
        "id": 3,
        "ingredients": [
            "Fiel salad",
            "egg"
        ],
        "imageUrl": "salad/FieldSaladWithEgg.jpg"
    },
    {
        "name": "Rocket with parmesan",
        "prize": 4,
        "id": 4,
        "ingredients": [
            "IcebRocketerg",
            "parmesan"
        ],
        "imageUrl": "salad/RocketWithParmesan.jpg"
    }
];


    var container = document.getElementById('saladContainer');
    var shoppingCartValue = 0;
    var totalPriceValue = 0;

function updateCartAndTotal() {
    shoppingCartValue++;
    shoppingCartElement.innerHTML = shoppingCartValue;
}
    
for (var i = 0; i < salads.length; i++) {
    var salad = salads[i];

    var htmlCode = `<div class="contentDiv">
        <img class="salad" src="${salad.imageUrl}" alt="${salad.name}">
        <div class="saladDiv">
            <h4 class="name">${salad.name}</h4>
            <p>${salad.ingredients.join(', ')}</p>
            <div class="saladAlign">
                <div class="salatSelect">
                    <select name="dressings">
                    <option >Italian dressing</option>
                    <option >French dressing</option>
                    </select>
                </div>
                <div class="saladPreis" >
                    <h4>${salad.prize}$</h4>
                    <div class="shoppingCartDiv">
                    <div id="id${salad.id}" class="shoppingCartDiv">
                    <button><img class="shoppingCartImg" dataPrice="${salad.prize}" src="pizza/shoppingcart.png"></button>
                </div>
                </div>
            </div>
        </div>
    </div>
    `;
    container.innerHTML += htmlCode;
}

var addToCartButtons = document.querySelectorAll(".shoppingCartImg");
var shoppingCartElement = document.getElementById("shoppingCartValue");
var totalPriceElement = document.getElementById("totalPriceValue");

addToCartButtons.forEach(function(button) {
    button.addEventListener("click", function() {
        var price = parseFloat(button.getAttribute("dataPrice"));
        totalPriceValue += price;
        totalPriceElement.innerHTML = totalPriceValue;
        updateCartAndTotal();
    });
});


