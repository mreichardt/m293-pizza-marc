




function clearShopingCart(){
    if (shoppingCartValue !== 0){
    totalPriceValue = 0;
    document.getElementById("totalPriceValue").textContent = totalPriceValue;
    shoppingCartValue = 0;
    document.getElementById("shoppingCartValue").textContent = shoppingCartValue;

    alert("Thanks for Shopping at Tony's Pizza factory");
}}


totalPriceElement.innerHTML = totalPriceValue;
shoppingCartElement.textContent = cartCount;

addToCartButtons.forEach(function (button) {
    button.addEventListener("click", function () {
        var price = parseFloat(button.getAttribute("dataPrice"));
        totalPriceValue += price;
        cartCount += 1;

        totalPriceElement.innerHTML = totalPriceValue;
        shoppingCartElement.textContent = cartCount;

        // Save total price and cart count in sessionStorage
        sessionStorage.setItem("savedTotalPrice", totalPriceValue);
        sessionStorage.setItem("savedCartCount", cartCount);
    });
});

