let softDrinks = [
    {
        "name": "Coke",
        "prize": 2,
        "id": 1,
        "size": [
            "330 ml",
            "0.5 cl",
            "1l"
        ],
        "imageUrl": "soft/coke.jpg"
    },
    {
        "name": "Fanta",
        "prize": 2,
        "id": 2,
        "size": [
            "330 ml",
            "0.5 cl",
            "1l"
        ],
        "imageUrl": "soft/fanta.jpg"
    },
    {
        "name": "Pepsi",
        "prize": 2,
        "id": 3,
        "size": [
            "330 ml",
            "0.5 cl",
            "1l"
        ],
        "imageUrl": "soft/pepsi.jpg"
    },
    {
        "name": "Red Bull",
        "prize": 3,
        "id": 4,
        "size": [
            "250 ml",
            "355 ml"
        ],
        "imageUrl": "soft/redbull.jpg"
    },
];

var container = document.getElementById('softDrinkContainer');
var shoppingCartValue = 0;
var totalPriceValue = 0;

function updateCartAndTotal() {
    shoppingCartValue++;
    shoppingCartElement.innerHTML = shoppingCartValue;
}

for (var i = 0; i < softDrinks.length; i++) {
    var softDrink = softDrinks[i];

    var htmlCode = `
    <div class="softDrink">
        <img src="${softDrink.imageUrl}" alt="${softDrink.name}">
        <h4>${softDrink.name}</h4>
        <div class="softDrinkAlign">
            <select class="sizeSelect">
                <option>${softDrink.size[0]}</option>
                <option>${softDrink.size[1]}</option>
                <option>${softDrink.size[2]}</option>
            </select>
            <div class="softDrinkPreis">
                <h4>${softDrink.prize} $</h4>
                <div id="id${softDrink.id}" class="shoppingCartDiv">
                    <button><img class="shoppingCartImg" dataPrice="${softDrink.prize}" src="pizza/shoppingcart.png"></button>
                </div>
            </div>
        </div>
    </div>
    `;

    container.innerHTML += htmlCode;
}

var addToCartButtons = document.querySelectorAll(".shoppingCartImg");
var shoppingCartElement = document.getElementById("shoppingCartValue");
var totalPriceElement = document.getElementById("totalPriceValue");

addToCartButtons.forEach(function(button) {
    button.addEventListener("click", function() {
        var price = parseFloat(button.getAttribute("dataPrice"));
        totalPriceValue += price;
        totalPriceElement.innerHTML = totalPriceValue;
        updateCartAndTotal();
        localStorage.setItem('savedNumber', totalPriceValue);
        localStorage.setItem('savedNumber', number);

    });
});


