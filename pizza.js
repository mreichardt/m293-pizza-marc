
let pizzas = [
    {
        "name": "Piccante",
        "prize": 16,
        "id": 1,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Spicy Salami",
            "Chilies",
            "Oregano"
        ],
        "imageUrl": "pizza/piccante.jpg"
    },
    {
        "name": "Giardino",
        "prize": 14,
        "id": 2,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Artichokes",
            "Fresh Mushrooms"
        ],
        "imageUrl": "pizza/giardino.jpg"
    },
    {
        "name": "Prosciuotto e funghi",
        "prize": 15,
        "id": 3,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Ham",
            "Fresh Mushrooms",
            "Oregano"
        ],
        "imageUrl": "pizza/prosciuttoefunghi.jpg"
    },
    {
        "name": "Quattro formaggi",
        "prize": 13,
        "id": 4,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Parmesan",
            "Gorgonzola"
        ],
        "imageUrl": "pizza/quattroformaggi.jpg"
    },
    {
        "name": "Quattro stagioni",
        "prize": 17,
        "id": 5,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Ham",
            "Artichokes",
            "Fresh Mushrooms"
        ],
        "imageUrl": "pizza/quattrostagioni.jpg"
    },
    {
        "name": "Stromboli",
        "prize": 12,
        "id": 6,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Fresh Chilies",
            "Olives",
            "Oregano"
        ],
        "imageUrl": "pizza/stromboli.jpg"
    },
    {
        "name": "Verde",
        "prize": 13,
        "id": 7,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Broccoli",
            "Spinach",
            "Oregano"
        ],
        "imageUrl": "pizza/verde.jpg"
    },
    {
        "name": "Rustica",
        "prize": 15,
        "id": 8,
        "ingredients": [
            "Tomato",
            "Mozzarella",
            "Ham",
            "Bacon",
            "Onions",
            "Garlic",
            "Oregano"
        ],
        "imageUrl": "pizza/rustica.jpg"
    }
];

var container = document.getElementById('pizzaContainer');

var shoppingCartValue = 0;
var totalPriceValue = 0;

function updateCartAndTotal() {
    shoppingCartValue++;
    shoppingCartElement.innerHTML = shoppingCartValue;
}


for (var i = 0; i < pizzas.length; i++) {
    var pizza = pizzas[i];

    var htmlCode = `<div class="pizzaDiv">
        <img class="pizza" src="${pizza.imageUrl}">
        <div class="pizzaPreis">
            <h4 class="name">${pizza.name}</h4>
            <div class="pizzaPreisAlign">
                <h4>${pizza.prize}$</h4>
                <div class="shoppingCartDiv">
                <div id="id${pizza.id}" class="shoppingCartDiv">
                    <button><img class="shoppingCartImg" dataPrice="${pizza.prize}" src="pizza/shoppingcart.png"></button>
                </div>
                </div>
            </div>
        </div>
        <p>${pizza.ingredients.join(', ')}</p>
        </div>
    `;

    container.innerHTML += htmlCode;
}

var addToCartButtons = document.querySelectorAll(".shoppingCartImg");
var shoppingCartElement = document.getElementById("shoppingCartValue");
var totalPriceElement = document.getElementById("totalPriceValue");

addToCartButtons.forEach(function(button) {
    button.addEventListener("click", function() {
        var price = parseFloat(button.getAttribute("dataPrice"));
        totalPriceValue += price;
        totalPriceElement.innerHTML = totalPriceValue;
        updateCartAndTotal();
        localStorage.setItem('savedNumber', totalPriceValue);
        localStorage.setItem('savedNumber', number);

    });
});
