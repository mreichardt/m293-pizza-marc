
function validateAndRedirect() {
    var nameInput = document.getElementById("benutzername");
    var emailInput = document.getElementById("e-mail");
    var likeInput = document.getElementById("like")

    if (nameInput.validity.valid && emailInput.validity.valid ) {
        window.location.href = "danke.html";
    } else {
        alert("Please fill in all required fields correctly.");
    }
}